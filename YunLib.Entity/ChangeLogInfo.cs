﻿//#######################
//#                     #
//#      流通日志       #
//#                     #
//#######################
//Author:纪钟磊
//CreateTime:2017/6/23

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yunlib.Entity
{
    public class ChangeLogInfo
    {
        /// <summary>
        /// 操作类型
        /// </summary>
        public string OperateType { get; set; }

        /// <summary>
        /// 条形码
        /// </summary>
        public string BarCode { get; set; }

        /// <summary>
        /// 登录号
        /// </summary>
        public string LoginAccount { get; set; }

        /// <summary>
        /// 读者条码
        /// </summary>
        public string ReaderCode { get; set; }

        /// <summary>
        /// 处理时间
        /// </summary>
        public DateTime DealTime { get; set; }

        /// <summary>
        /// 赔罚款
        /// </summary>
        public decimal Penalty { get; set; }

        /// <summary>
        /// 操作员
        /// </summary>
        public int Operator { get; set; }

        /// <summary>
        /// 主键码
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// 流通库室键码
        /// </summary>
        public int LTKID { get; set; }

        /// <summary>
        /// 签到键码
        /// </summary>
        public int QDID { get; set; }
    }
}
