﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YunLib.API.Models
{
    public class BackMessageModel3
    {
        public string CODE { get; set; }
        public string MESSAGE { get; set; }
        public ReaderLoginModel DATA { get; set; }
    }
}