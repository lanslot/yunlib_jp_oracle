﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YunLib.API.Models
{
    public class SearchToPageModel
    {
        public string searchType { get; set; }
        public string searchStr { get; set; }
        public string page { get; set; }
        public string pageSize { get; set; }
        public string timesstamp { get; set; }
        public string sign { get; set; }

    }
}