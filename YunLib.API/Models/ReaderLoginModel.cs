﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YunLib.API.Models
{
    public class ReaderLoginModel
    {
        public string DZ_CODE{get;set;}
        public string PASS_WORD {get;set;}
        public decimal CAN_LEND {get;set;}
        public string DZ_PHONE { get; set; }
        public DateTime LOSE_TIME { get; set; }
        public string DZ_ADDRESS { get; set; }
        public decimal DUE_MONEY {get;set;}
        public string DZ_GENDER {get;set;}
        public string DZ_NAME { get; set; }
        public string LEND_CODE {get;set;}
        public string NATION {get;set;}
        public decimal UNDER_MONEY {get;set;}
        public string DZ_UNIT { get; set; }
        public DateTime ADD_TIME { get; set; }
        public string DZ_LEVEL { get; set; }
        public string ID_CARD { get; set; }
        public string DZ_EMAIL { get; set; }
        public decimal HAS_LEND { get; set; }
        public decimal DZ_YAJIN { get; set; }
        public string CRED_TYPE { get; set; }

    }
}