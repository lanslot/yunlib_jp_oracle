﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YunLib.API.Models
{
    public class SearchModel
    {
        public string dz_code { get; set; }
        public string book_code { get; set; }
        public string code { get; set; }
        public string timesstamp { get; set; }
        public string sign { get; set; }

    }
}