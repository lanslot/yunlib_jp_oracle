﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YunLib.API.Models
{
    public class BackMessageModel<T> where T : new()
    {
        public string CODE { get; set; }
        public string MESSAGE { get; set; }
        public IList<T> DATA { get; set; }
    }
}