﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YunLib.API.Models
{
    public class BackLendModel
    {
        public string TITLE { get; set; }
        public string AUTHOR { get; set; }
        public string BOOKCODE { get; set; }
        public DateTime PROCESSINGTIME { get; set; }
        public DateTime RETURNTIME { get; set; }
        public int RENEWNUMBER{ get; set; }
        public string DZ_UNIT { get; set; }
    }
}