﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YunLib.API.Models
{
    public class BookInfoModel
    {
        public string PUBLISHER { get; set; }
        public int PAGES { get; set; }
        public string LAGS { get; set; }
        public decimal PRICE { get; set; }
        public string ISBN { get; set; }
        public string TITLE { get; set; }
        public string AUTHOR { get; set; }
        public string PUBDATE { get; set; }
        public string CLASSIFICATION_NUM { get; set; }
        public string EDITION { get; set; }
        public string PUBLISHERPLACE { get; set; }

    }
}