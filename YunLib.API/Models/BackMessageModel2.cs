﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YunLib.API.Models
{
    public class BackMessageModel2
    {
        public string CODE { get; set; }
        public string MESSAGE { get; set; }
        public BookInfoModel DATA { get; set; }
    }
}