﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YunLib.API.Models
{
    public class ChangeBookModel
    {
        public DateTime CHECKTIME { get; set; }
        public int LENDKEY { get; set; }
        public int RENEWNUMBER { get; set; }
        public long SIGNIN { get; set; }
        public string DZCODE { get; set; }
        public string LOGIN { get; set; }
        public DateTime PROCESSINGTIME { get; set; }
        public string BARCODE { get; set; }
        public int VIRTUALLIBRARYROOM { get; set; }
        public DateTime RETURNTIME { get; set; }
        public long PRIMARYKEY { get; set; }

    }
}