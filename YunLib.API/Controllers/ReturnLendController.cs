﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Yunlib.Common;
using Yunlib.Extensions;
using YunLib.API.Models;
using YunLib.BLL;
using YunLib.Common;

namespace YunLib.API.Controllers
{
    public class ReturnLendController : ApiController
    {

        private ReturnOrBrBLL rob = new ReturnOrBrBLL();
        private ReaderInfoBLL rib = new ReaderInfoBLL();
        private string loginKey = ConfigManager.LoginKey;
        // GET: api/ReturnLend
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/ReturnLend/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/ReturnLend
        public JObject Post(SearchModel model)
        {
            BackMessageModel<ChangeBookModel> bm = new BackMessageModel<ChangeBookModel>();
            bm.CODE = "400";
            try
            {
                var signstr = "{0}{1}{2}{3}".FormatWith(model.book_code, model.dz_code, model.timesstamp, loginKey);
                var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signstr).ToLower());
                if (sign != model.sign)
                {
                    bm.MESSAGE = "密匙错误";
                    return bm.ToJson().ToJObject();
                }
                else
                {
                    DataTable dt = new DataTable();
                    if (string.IsNullOrWhiteSpace(model.dz_code) && string.IsNullOrWhiteSpace(model.book_code))
                    {
                        bm.MESSAGE = "请输入正确的读者条码和书籍条码";
                        return bm.ToJson().ToJObject();
                    }
                    else if (!string.IsNullOrWhiteSpace(model.dz_code) && string.IsNullOrWhiteSpace(model.book_code))
                    {
                        dt = rob.GetChangeInfoByDzCode(model.dz_code);
                    }
                    else if (string.IsNullOrWhiteSpace(model.dz_code) && !string.IsNullOrWhiteSpace(model.book_code))
                    {
                        dt = rob.GetChangeInfoByBarcode(model.book_code);
                    }
                    else
                    {
                        dt = rob.GetChangeInfo(model.dz_code, model.book_code);
                    }

                    if (dt.Rows.Count > 0)
                    {
                        var list = DataTable2List<ChangeBookModel>.ConvertToModel(dt);
                        bm.CODE = "0000";
                        bm.MESSAGE = "成功";
                        bm.DATA = list;
                    }
                    else
                    {
                        bm.MESSAGE = "没有借阅记录";
                    }

                    return bm.ToJson().ToJObject();
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorRecord("【{0}】:{1}".FormatWith(DateTime.Now, ex.Message));
                bm.MESSAGE = ex.Message;
                return bm.ToJson().ToJObject();

            }
        }

        // PUT: api/ReturnLend/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/ReturnLend/5
        public void Delete(int id)
        {
        }
    }
}
