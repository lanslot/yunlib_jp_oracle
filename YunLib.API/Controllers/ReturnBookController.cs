﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Yunlib.Common;
using Yunlib.Entity;
using Yunlib.Extensions;
using YunLib.API.Models;
using YunLib.BLL;
using YunLib.Common;

namespace YunLib.API.Controllers
{
    public class ReturnBookController : ApiController
    {
        private ReaderInfoBLL rib = new ReaderInfoBLL();
        private ReturnOrBrBLL BLL = new ReturnOrBrBLL();
        private string loginKey = ConfigManager.LoginKey;
        // GET: api/ReturnBook
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/ReturnBook/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/ReturnBook
        public JObject Post(SearchModel model1)
        {
            BackMessageModel<BookInfoModel> bm = new BackMessageModel<BookInfoModel>();
            bm.CODE = "400";
            try
            {
                var signstr = "{0}{1}{2}".FormatWith(model1.book_code, model1.timesstamp, loginKey);
                var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signstr).ToLower());
                if (sign != model1.sign)
                {
                    bm.MESSAGE = "密匙错误";
                    return bm.ToJson().ToJObject();
                }
                else
                {
                    string barCode = model1.book_code;

                    ChangeKuInfo model = BLL.GetBorrowBooksInfoByBarcode(barCode);
                    if (model == null)
                    {
                        bm.MESSAGE = "没有借阅记录";
                        return bm.ToJson().ToJObject();
                    }

                    var book = BLL.GetBooksInfoByBarcode(barCode);
                    BLL.updateBookTypeByBarCode(barCode, 0);
                    //流通库中有这本书,就把流通库中记录删除
                    //在流通日志中新增数据
                    ChangeLogInfo logModel = new ChangeLogInfo
                    {
                        OperateType = "H",
                        BarCode = barCode,
                        LoginAccount = barCode,
                        ReaderCode = book.ReaderCode,
                        DealTime = DateTime.Now,
                        Penalty = 0.00m,
                        Operator = 99,
                        ID = book.ID
                    };


                    DataTable user = rib.GetUserByReaderCode(book.ReaderCode);

                    //还书成功，读者已借书籍书减少1本
                    rib.UpdateUserBoorowNum(int.Parse(user.Rows[0][1].ToString()) - 1, book.ReaderCode);

                 


                    bool res = BLL.DeleteChangeInfoByBarCode(logModel);

                    if (res)
                    {
                        bm.MESSAGE = "还书成功";
                        bm.CODE = "0000";
                        return bm.ToJson().ToJObject();
                    }
                    else
                    {
                        bm.MESSAGE = "图书条码或者读者条码错误";
                        return bm.ToJson().ToJObject();
                    }

                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorRecord("【{0}】:{1}".FormatWith(DateTime.Now, ex.Message));
                bm.MESSAGE = ex.Message;
                return bm.ToJson().ToJObject();

            }
        }

        // PUT: api/ReturnBook/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/ReturnBook/5
        public void Delete(int id)
        {
        }
    }
}
