﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Yunlib.Common;
using Yunlib.Entity;
using Yunlib.Extensions;
using YunLib.API.Models;
using YunLib.BLL;
using YunLib.Common;

namespace YunLib.API.Controllers
{
    public class BorrowBooksController : ApiController
    {
        private ReturnOrBrBLL rob = new ReturnOrBrBLL();
        private ReaderInfoBLL rib = new ReaderInfoBLL();
        private string loginKey = ConfigManager.LoginKey;

        // GET: api/BorrowBooks
        //[HttpGet]
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        
        // GET: api/BorrowBooks/5
        [HttpGet]
        public JObject Get(string code,string dz_code,string book_code,string timesstamp,string sign)
        {
            BackMessageModel<BookInfoModel> bm = new BackMessageModel<BookInfoModel>();
            bm.CODE = "400";
            var signstr = "{0}{1}{2}{3}{4}".FormatWith(code, book_code, dz_code, timesstamp, loginKey);
            var sign1 = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signstr).ToLower());
            if (sign1 != sign)
            {
                bm.MESSAGE = "密匙错误";
                return bm.ToJson().ToJObject();
            }
            else
            {
                if (code == "0")
                {
                    DataTable dtUser = rib.GetUser(dz_code);
                    if (Convert.ToDateTime(dtUser.Rows[0]["失效日期"].ToString()) < DateTime.Now)
                    {
                        //读者卡失效了
                        bm.MESSAGE = "读者卡已在：{0} 失效".FormatWith(dtUser.Rows[0]["失效日期"].ToString());
                        return bm.ToJson().ToJObject();
                    }
                    else
                    {
                        //读者卡没有失效,就判断密码是否输入正确
                        if (dtUser.Rows.Count > 0 && dtUser != null)
                        {
                            bool HasExpire = false;
                            DataTable dt1 = rob.GetCirculationByUserId(dz_code);
                            var list = DataTable2List<ChangeKuInfo>.ConvertToModel(dt1);
                            foreach (var item in list)
                            {
                                var reTime = item.ShouldReturnTime;
                                if (reTime < DateTime.Now)
                                {
                                    HasExpire = true;
                                }
                            }
                            if (HasExpire)
                            {
                                bm.MESSAGE = "你有过期书籍";
                                return bm.ToJson().ToJObject();
                            }
                            else
                            {
                                if ((int)dtUser.Rows[0]["可外借"] - (int)dtUser.Rows[0]["已外借"] <= 0)
                                {
                                    bm.MESSAGE = "可外借数量已达上限";
                                    return bm.ToJson().ToJObject();
                                }
                                else
                                {

                                    var book = rob.GetBooksInfoByBarcode(book_code);

                                    ChangeLogInfo logInfo = new ChangeLogInfo
                                    {
                                        OperateType = "J",
                                        BarCode = book_code,
                                        LoginAccount = book_code,
                                        DealTime = DateTime.Now,
                                        ID = book.ID,
                                        Penalty = 0,
                                        ReaderCode = dtUser.Rows[0][2].ToString(),
                                        Operator = 99
                                    };

                                    //插入流通日志
                                    rob.InsertLTKOneInfo(logInfo);

                                    ChangeKuInfo kuInfo = new ChangeKuInfo
                                    {

                                        ID = book.ID,
                                        BarCode = book_code,
                                        LoginAccount = book_code,
                                        BorrowTime = DateTime.Now,
                                        ReaderCode = dtUser.Rows[0][1].ToString(),
                                        OnceBorrowCount = rob.GetOnceBorrowCount(dtUser.Rows[0][2].ToString()),
                                        ShouldReturnTime = DateTime.Now.AddMonths(1)
                                    };
                                    //插入流通库
                                    rob.AddCirculation(kuInfo);

                                    rib.UpdateUserBoorowNum((int)dtUser.Rows[0][1] + 1, dz_code);
                                    bm.MESSAGE = "成功";
                                    bm.CODE = "0000";
                                    return bm.ToJson().ToJObject();
                                }
                            }
                        }
                    }

                }
                else if (code == "1")
                {
                    var book = rob.GetBooksInfoByBarcode(book_code);
                    //配置Entity
                    ChangeLogInfo model1 = new ChangeLogInfo
                    {
                        OperateType = "X",
                        BarCode = book_code,
                        LoginAccount = book_code,
                        ReaderCode = book.ReaderCode,
                        DealTime = DateTime.Now,
                        Penalty = 0.00m,
                        Operator = 99,
                        ID = book.ID
                    };


                    //在流通库中新增一条记录
                    bool res = rob.InsertLTKOneInfo(model1);
                    if (res)
                    {
                        bm.MESSAGE = "成功";
                        bm.CODE = "0000";
                        return bm.ToJson().ToJObject();
                    }
                    else
                    {
                        bm.MESSAGE = "失败";
                        return bm.ToJson().ToJObject();
                    }
                }
                else
                {
                    bm.MESSAGE = "操作类型错误";
                    return bm.ToJson().ToJObject();
                }
                return bm.ToJson().ToJObject();
            }
        }

        //[HttpPost]
        //public JObject Post(string code, string dz_code, string book_code, string timesstamp, string sign)
        //{
        //    LogManager.ErrorRecord("111");
        //    BackMessageModel<BookInfoModel> bm = new BackMessageModel<BookInfoModel>();
        //    bm.CODE = "400";
        //    var signstr = "{0}{1}{2}{3}{4}".FormatWith(code, book_code, dz_code, timesstamp, loginKey);
        //    var sign1 = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signstr).ToLower());
        //    if (sign1 != sign)
        //    {
        //        bm.MESSAGE = "密匙错误";
        //        return bm.ToJson().ToJObject();
        //    }
        //    else
        //    {
        //        if (code == "0")
        //        {
        //            LogManager.ErrorRecord("111");
        //            DataTable dtUser = rib.GetUser(dz_code);
        //            LogManager.ErrorRecord("222");
        //            if (Convert.ToDateTime(dtUser.Rows[0]["失效日期"].ToString()) < DateTime.Now)
        //            {
        //                //读者卡失效了
        //                bm.MESSAGE = "读者卡已在：{0} 失效".FormatWith(dtUser.Rows[0]["失效日期"].ToString());
        //                return bm.ToJson().ToJObject();
        //            }
        //            else
        //            {
        //                //读者卡没有失效,就判断密码是否输入正确
        //                if (dtUser.Rows.Count > 0 && dtUser != null)
        //                {
        //                    bool HasExpire = false;
        //                    DataTable dt1 = rob.GetCirculationByUserId(dz_code);
        //                    LogManager.ErrorRecord("333");
        //                    var list = DataTable2List<ChangeKuInfo>.ConvertToModel(dt1);
        //                    foreach (var item in list)
        //                    {
        //                        var reTime = item.ShouldReturnTime;
        //                        if (reTime < DateTime.Now)
        //                        {
        //                            HasExpire = true;
        //                        }
        //                    }
        //                    if (HasExpire)
        //                    {
        //                        bm.MESSAGE = "你有过期书籍";
        //                        return bm.ToJson().ToJObject();
        //                    }
        //                    else
        //                    {
        //                        if ((int)dtUser.Rows[0]["可外借"] - (int)dtUser.Rows[0]["已外借"] <= 0)
        //                        {
        //                            bm.MESSAGE = "可外借数量已达上限";
        //                            return bm.ToJson().ToJObject();
        //                        }
        //                        else
        //                        {
        //                            LogManager.ErrorRecord("444");
        //                            var book = rob.GetBooksInfoByBarcode(book_code);
        //                            LogManager.ErrorRecord("555");
        //                            ChangeLogInfo logInfo = new ChangeLogInfo
        //                            {
        //                                OperateType = "J",
        //                                BarCode = book_code,
        //                                LoginAccount = book_code,
        //                                DealTime = DateTime.Now,
        //                                ID = book.ID,
        //                                Penalty = 0,
        //                                ReaderCode = dtUser.Rows[0][2].ToString(),
        //                                Operator = 99
        //                            };
        //                            LogManager.ErrorRecord("666");
        //                            //插入流通日志
        //                            rob.InsertLTKOneInfo(logInfo);

        //                            ChangeKuInfo kuInfo = new ChangeKuInfo
        //                            {

        //                                ID = book.ID,
        //                                BarCode = book_code,
        //                                LoginAccount = book_code,
        //                                BorrowTime = DateTime.Now,
        //                                ReaderCode = dtUser.Rows[0][1].ToString(),
        //                                OnceBorrowCount = rob.GetOnceBorrowCount(dtUser.Rows[0][1].ToString()),
        //                                ShouldReturnTime = DateTime.Now.AddMonths(1)
        //                            };
        //                            //插入流通库
        //                            rob.AddCirculation(kuInfo);

        //                            rib.UpdateUserBoorowNum((int)dtUser.Rows[0]["已外借"] + 1, dz_code);
        //                            bm.MESSAGE = "成功";
        //                            bm.CODE = "0000";
        //                            return bm.ToJson().ToJObject();
        //                        }
        //                    }
        //                }
        //            }

        //        }
        //        else if (code == "1")
        //        {
        //            var book = rob.GetBooksInfoByBarcode(book_code);
        //            //配置Entity
        //            ChangeLogInfo model1 = new ChangeLogInfo
        //            {
        //                OperateType = "X",
        //                BarCode = book_code,
        //                LoginAccount = book_code,
        //                ReaderCode = book.ReaderCode,
        //                DealTime = DateTime.Now,
        //                Penalty = 0.00m,
        //                Operator = 99,
        //                ID = book.ID
        //            };


        //            //在流通库中新增一条记录
        //            bool res = rob.InsertLTKOneInfo(model1);
                    
        //            if (res)
        //            {
        //                bm.MESSAGE = "成功";
        //                bm.CODE = "0000";
        //                return bm.ToJson().ToJObject();
        //            }
        //            else
        //            {
        //                bm.MESSAGE = "失败";
        //                return bm.ToJson().ToJObject();
        //            }
        //        }
        //        else
        //        {
        //            bm.MESSAGE = "操作类型错误";
        //            return bm.ToJson().ToJObject();
        //        }
        //        return bm.ToJson().ToJObject();
        //    }
        //}

        // POST: api/BorrowBooks
        [HttpPost]
        public JObject Post(SearchModel model)
        {
            BackMessageModel<BookInfoModel> bm = new BackMessageModel<BookInfoModel>();
            bm.CODE = "400";
            try
            {
               
                var signstr = "{0}{1}{2}{3}{4}".FormatWith(model.code, model.book_code, model.dz_code, model.timesstamp, loginKey);
                var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signstr).ToLower());
                if(sign != model.sign)
                {
                    bm.MESSAGE = "密匙错误";
                    return bm.ToJson().ToJObject();
                }
                else
                {
                    if (model.code == "0")
                    {
                        LogManager.ErrorRecord("111");
                        DataTable dtUser = rib.GetUser(model.dz_code);
                        if (dtUser.Rows.Count <= 0)
                        {
                            bm.MESSAGE = "读者条码错误";
                            return bm.ToJson().ToJObject();
                        }
                        LogManager.ErrorRecord("222");
                        if (Convert.ToDateTime(dtUser.Rows[0]["失效日期"].ToString()) < DateTime.Now)
                        {
                            //读者卡失效了
                            bm.MESSAGE = "读者卡已在：{0} 失效".FormatWith(dtUser.Rows[0]["失效日期"].ToString());
                            return bm.ToJson().ToJObject();
                        }
                        else
                        {
                            //读者卡没有失效,就判断密码是否输入正确
                            if (dtUser.Rows.Count > 0 && dtUser != null)
                            {
                                bool HasExpire = false;
                                LogManager.ErrorRecord("333");
                                DataTable dt1 = rob.GetCirculationByUserId(model.dz_code);
                                LogManager.ErrorRecord("444");
                                var list = DataTable2List<ChangeKuInfo>.ConvertToModel(dt1);
                                foreach (var item in list)
                                {
                                    var reTime = item.ShouldReturnTime;
                                    if (reTime < DateTime.Now)
                                    {
                                        HasExpire = true;
                                    }
                                }
                                if (HasExpire)
                                {
                                    bm.MESSAGE = "你有过期书籍";
                                    return bm.ToJson().ToJObject();
                                }
                                else
                                {
                                    LogManager.ErrorRecord("555");
                                    if (int.Parse(dtUser.Rows[0]["可外借"].ToString() )- int.Parse(dtUser.Rows[0]["已外借"].ToString()) <= 0)
                                    {
                                        bm.MESSAGE = "可外借数量已达上限";
                                        return bm.ToJson().ToJObject();
                                    }
                                    else
                                    {
                                        LogManager.ErrorRecord("6666");
                                        var bb = rob.GetBorrowBooksInfoByBarcode(model.book_code);
                                        if (bb != null) {
                                            bm.MESSAGE = "该书已借阅";
                                            return bm.ToJson().ToJObject();
                                        }

                                        var book = rob.GetBooksInfoByBarcode(model.book_code);
                                      
                                        ChangeLogInfo logInfo = new ChangeLogInfo
                                        {
                                            OperateType = "J",
                                            BarCode = model.book_code,
                                            LoginAccount = model.book_code,
                                            DealTime = DateTime.Now,
                                            ID = book.ID,
                                            Penalty = 0,
                                            ReaderCode = dtUser.Rows[0][1].ToString(),
                                            Operator = 99
                                        };

                                        //插入流通日志
                                       bool flag = rob.InsertLTKOneInfo(logInfo);
                                        if (!flag) {
                                            bm.MESSAGE = "插入流通日志失败";
                                            return bm.ToJson().ToJObject();
                                        }
                                        rob.updateBookTypeByBarCode(model.book_code, 1);
                                        ChangeKuInfo kuInfo = new ChangeKuInfo
                                        {

                                            ID = book.ID,
                                            BarCode = model.book_code,
                                            LoginAccount = model.book_code,
                                            BorrowTime = DateTime.Now,
                                            ReaderCode = dtUser.Rows[0][1].ToString(),
                                            OnceBorrowCount = rob.GetOnceBorrowCount(dtUser.Rows[0][2].ToString()),
                                            ShouldReturnTime = DateTime.Now.AddMonths(1)
                                        };
                                        //插入流通库
                                        bool flag2 =  rob.AddCirculation(kuInfo);
                                        if (!flag2)
                                        {
                                            bm.MESSAGE = "插入流通记录失败";
                                            return bm.ToJson().ToJObject();
                                        }
                                        rib.UpdateUserBoorowNum(int.Parse(dtUser.Rows[0]["已外借"].ToString()) + 1, model.dz_code);
                                        bm.MESSAGE = "成功";
                                        bm.CODE = "0000";
                                        return bm.ToJson().ToJObject();
                                    }
                                }
                            }
                        }

                    }
                    else if (model.code == "1")
                    {
                        ChangeKuInfo ck = rob.GetBorrowBooksInfoByBarcode(model.book_code);

                        if (ck == null)
                        {
                            bm.MESSAGE = "没有借阅记录";
                            return bm.ToJson().ToJObject();
                        }
                        if(ck.OnceBorrowCount >= 5)
                        {
                            bm.MESSAGE = "超出借阅次数";
                            return bm.ToJson().ToJObject();
                        }

                        bool flag = rob.UpdateChangeKUReBackTime(model.book_code, ck.ShouldReturnTime.AddDays(30),ck.OnceBorrowCount+1);
                        if (!flag)
                        {
                            bm.MESSAGE = "修改归还时间出错";
                            return bm.ToJson().ToJObject();
                        }
                        var book = rob.GetBooksInfoByBarcode(model.book_code);
                        //配置Entity
                        ChangeLogInfo model1 = new ChangeLogInfo
                        {
                            OperateType = "X",
                            BarCode = model.book_code,
                            LoginAccount = model.book_code,
                            ReaderCode = book.ReaderCode,
                            DealTime = DateTime.Now,
                            Penalty = 0.00m,
                            Operator = 99,
                            ID = book.ID
                        };

                       
                        //在流通库中新增一条记录
                        bool res = rob.InsertLTKOneInfo(model1);
                        if (res)
                        {
                            bm.MESSAGE = "成功";
                            bm.CODE = "0000";
                            return bm.ToJson().ToJObject();
                        }
                        else
                        {
                            bm.MESSAGE = "失败";
                            return bm.ToJson().ToJObject();
                        }
                    }
                    else
                    {
                        bm.MESSAGE = "操作类型错误";
                        return bm.ToJson().ToJObject();
                    }
                    return bm.ToJson().ToJObject();
                }
            }catch (Exception ex)
            {
                LogManager.ErrorRecord("【{0}】:{1}".FormatWith(DateTime.Now, ex.Message));
                bm.MESSAGE = ex.StackTrace;
                return bm.ToJson().ToJObject();

            }
        }

        // PUT: api/BorrowBooks/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/BorrowBooks/5
        public void Delete(int id)
        {
        }
    }
}
