﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Yunlib.Common;
using Yunlib.Extensions;
using YunLib.API.Models;
using YunLib.BLL;
using YunLib.Common;

namespace YunLib.API.Controllers
{
    public class UserLoginController : ApiController
    {
        private ReaderInfoBLL rib = new ReaderInfoBLL();
        private string loginKey = ConfigManager.LoginKey;
        // GET: api/UserLogin
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Login_check/5
        public JObject Get(string dz_code, string pass)
        {
            BackMessageModel3 bm = new BackMessageModel3();
            bm.CODE = "400";
            try
            {
                DataTable dt = rib.UserLogin(dz_code);
                if (dt.Rows.Count > 0)
                {

                    var list = DataTable2List<ReaderLoginModel>.ConvertToModel(dt);
                    var pwd = "{0}{1}".FormatWith(list[0].PASS_WORD.Trim(), loginKey);
                    // var signPwd = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(pwd).ToLower());
                    var signPwd = Md5.md5(pwd, 32).ToLower();
                    if (pass == signPwd)
                    {
                        bm.CODE = "0000";
                        bm.MESSAGE = "成功";
                        bm.DATA = list[0];
                    }
                    else
                    {
                        bm.MESSAGE = "密码错误";
                    }
                }
                else
                {
                    bm.MESSAGE = "用户不存在";
                }

                return bm.ToJson().ToJObject();
            }
            catch (Exception ex)
            {
                LogManager.ErrorRecord("【{0}】:{1}".FormatWith(DateTime.Now, ex.Message));
                bm.MESSAGE = ex.Message;
                return bm.ToJson().ToJObject();

            }
        }

        // POST: api/UserLogin
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/UserLogin/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/UserLogin/5
        public void Delete(int id)
        {
        }
    }
}
