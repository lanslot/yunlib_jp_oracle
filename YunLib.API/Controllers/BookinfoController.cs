﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Yunlib.Common;
using Yunlib.Extensions;
using YunLib.API.Models;
using YunLib.BLL;
using YunLib.Common;

namespace YunLib.API.Controllers
{
    public class BookinfoController : ApiController
    {
        private ReaderInfoBLL rib = new ReaderInfoBLL();
        private string loginKey = ConfigManager.LoginKey;
        // GET: api/Bookinfo
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Bookinfo/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Bookinfo
        [HttpPost]
        public JObject Post(SearchModel model)
        {
            BackMessageModel2 bm = new BackMessageModel2();
            bm.CODE = "400";
            try
            {
               
                var signstr = "{0}{1}{2}".FormatWith(model.book_code, model.timesstamp, loginKey);
                var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signstr).ToLower());
                if(sign != model.sign)
                {
                    bm.MESSAGE = "密匙错误";
                }
                else
                {
                    ReturnOrBrBLL rob = new ReturnOrBrBLL();
                    DataTable dt = rob.GetNewBooksInfoByBarcode(model.book_code);

                    if (dt.Rows.Count > 0)
                    {
                        var list = DataTable2List<BookInfoModel>.ConvertToModel(dt);
                        bm.CODE = "0000";
                        bm.MESSAGE = "成功";
                        bm.DATA = list[0];
                    }
                    else
                    {
                        bm.MESSAGE = "图书条形码错误";
                    }
                }

                return bm.ToJson().ToJObject();
            }catch(Exception ex)
            {
                LogManager.ErrorRecord("【{0}】:{1}".FormatWith(DateTime.Now, ex.Message));
                bm.MESSAGE = ex.Message;
                return bm.ToJson().ToJObject();

            }
        }

        // PUT: api/Bookinfo/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Bookinfo/5
        public void Delete(int id)
        {
        }
    }
}
