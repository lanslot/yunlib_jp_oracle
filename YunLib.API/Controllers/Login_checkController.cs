﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Yunlib.Common;
using Yunlib.Extensions;
using YunLib.API.Models;
using YunLib.BLL;
using YunLib.Common;

namespace YunLib.API.Controllers
{
    public class Login_checkController : ApiController
    {
        private ReaderInfoBLL rib = new ReaderInfoBLL();
        private string loginKey = ConfigManager.LoginKey;
        // GET: api/Login_check
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Login_check/5
        public JObject Get(string dz_code,string pass)
        {
            BackMessageModel3 bm = new BackMessageModel3();
            bm.CODE = "400";
            try
            {
                DataTable dt = rib.UserLogin(dz_code);
                if (dt.Rows.Count > 0)
                {

                    var list = DataTable2List<ReaderLoginModel>.ConvertToModel(dt);
                    var pwd = "{0}{1}".FormatWith(list[0].PASS_WORD.Trim(), loginKey);
                    var signPwd = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(pwd).ToLower());
                    if (!string.IsNullOrWhiteSpace(list[0].PASS_WORD.Trim()))
                    {


                        if (pass == signPwd)
                        {
                            bm.CODE = "0000";
                            bm.MESSAGE = "成功";
                            bm.DATA = list[0];
                        }
                        else
                        {
                            bm.MESSAGE = "密码错误";
                        }
                    }
                    else
                    {
                        bm.MESSAGE = "密码为空，请到图书馆设置密码";
                    }
                }
                else
                {
                    bm.MESSAGE = "用户不存在";
                }

                return bm.ToJson().ToJObject();
            }
            catch (Exception ex)
            {
                LogManager.ErrorRecord("【{0}】:{1}".FormatWith(DateTime.Now, ex.Message));
                bm.MESSAGE = ex.Message;
                return bm.ToJson().ToJObject();

            }
        }

        // POST: api/Login_check
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Login_check/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Login_check/5
        public void Delete(int id)
        {
        }
    }
}
