﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Yunlib.Common;
using Yunlib.Extensions;
using YunLib.API.Models;
using YunLib.BLL;
using YunLib.Common;

namespace YunLib.API.Controllers
{
    public class SearchBookController : ApiController
    {
        private ReturnOrBrBLL rob = new ReturnOrBrBLL();
        private ReaderInfoBLL rib = new ReaderInfoBLL();
        private string loginKey = ConfigManager.LoginKey;
        // GET: api/SearchBook
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/SearchBook/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/SearchBook
        public JObject Post(SearchToPageModel model)
        {
            BackMessageModel<BookInfoModel> bm = new BackMessageModel<BookInfoModel>();
            bm.CODE = "400";
            try
            {
                var signstr = "{0}{1}{2}{3}{4}{5}".FormatWith(model.searchType, model.searchStr, model.page, model.pageSize, model.timesstamp, loginKey);
                var sign = EncrypAndDecryp.Base64Encode(EncrypAndDecryp.SHA1(signstr).ToLower());
                if (sign == model.sign)
                {
                    bm.MESSAGE = "密匙错误";
                    return bm.ToJson().ToJObject();
                }
                else
                {
                    DataTable dt = rob.GetNewBooksInfoByPage(model.searchType, model.searchStr, int.Parse(model.page), int.Parse(model.pageSize));


                    if (dt.Rows.Count > 0)
                    {
                        var list = DataTable2List<BookInfoModel>.ConvertToModel(dt);
                        bm.CODE = "0000";
                        bm.MESSAGE = "成功";
                        bm.DATA = list;
                    }
                    else
                    {
                        bm.MESSAGE = "没有书籍记录";
                    }
                    return bm.ToJson().ToJObject();
                }
            }
            catch (Exception ex)
            {
                LogManager.ErrorRecord("【{0}】:{1}".FormatWith(DateTime.Now, ex.Message));
                bm.MESSAGE = ex.Message;
                return bm.ToJson().ToJObject();

            }
        }

        // PUT: api/SearchBook/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/SearchBook/5
        public void Delete(int id)
        {
        }
    }
}
