﻿using System;
using System.Configuration;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Oracle.ManagedDataAccess.Client;

namespace Yunlib.DAL
{

    /// <summary>  
    /// A helper class used to execute queries against an Oracle database  
    /// </summary>  
    public class OracleHelper
    {

        public static readonly string strConn = ConfigurationManager.ConnectionStrings["WayLibDB"].ConnectionString.ToString();
        
    


        /// <summary>
        /// 执行sql获取数据集
        /// </summary>
        /// <param name="cmdText">sql语句</param>
        /// <param name="oracleParameters">所传参数（必须按照存储过程参数顺序）</param>
        /// <param name="strConn">链接字符串</param>
        /// <returns></returns>
        public static DataTable ExecToSqlGetTable(string cmdText, OracleParameter[] oracleParameters)
        {
            using (OracleConnection conn = new OracleConnection(strConn))
            {
                OracleCommand cmd = new OracleCommand(cmdText, conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddRange(oracleParameters);
                OracleDataAdapter oda = new OracleDataAdapter(cmd);
                conn.Open();
                DataSet ds = new DataSet();
                oda.Fill(ds);
                conn.Close();
                return ds.Tables[0];
            }
        }

        /// <summary>
        /// 执行sql获取数据集
        /// </summary>
        /// <param name="cmdText">sql语句</param>
        /// <param name="oracleParameters">所传参数（必须按照存储过程参数顺序）</param>
        /// <param name="strConn">链接字符串</param>
        /// <returns></returns>
        public static DataTable ExecToSqlGetTable(string cmdText)
        {
            using (OracleConnection conn = new OracleConnection(strConn))
            {
                OracleCommand cmd = new OracleCommand(cmdText, conn);
                cmd.CommandType = CommandType.Text;
                OracleDataAdapter oda = new OracleDataAdapter(cmd);
                conn.Open();
                DataSet ds = new DataSet();
                oda.Fill(ds);
                conn.Close();
                return ds.Tables[0];
            }
        }

        /// <summary>
        /// 执行sql执行增删改
        /// </summary>
        /// <param name="cmdText">sql语句</param>
        /// <param name="oracleParameters">所传参数（必须按照存储过程参数顺序）</param>
        /// <param name="strConn">链接字符串</param>
        /// <returns></returns>
        public static int ExecToSqlNonQuery(string cmdText, OracleParameter[] oracleParameters)
        {
            using (OracleConnection conn = new OracleConnection(strConn))
            {
                OracleCommand cmd = new OracleCommand(cmdText, conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddRange(oracleParameters);
                conn.Open();
                int result = cmd.ExecuteNonQuery();
                conn.Close();
                return result;
            }
        }
        /// <summary>
        /// 执行sql执行增删改
        /// </summary>
        /// <param name="cmdText">sql语句</param>
        /// <param name="oracleParameters">所传参数（必须按照存储过程参数顺序）</param>
        /// <param name="strConn">链接字符串</param>
        /// <returns></returns>
        public static int ExecToSqlNonQuery(string cmdText)
        {
            using (OracleConnection conn = new OracleConnection(strConn))
            {
                OracleCommand cmd = new OracleCommand(cmdText, conn);
                cmd.CommandType = CommandType.Text;
                conn.Open();
                int result = cmd.ExecuteNonQuery();
                conn.Close();
                return result;
            }
        }

        public static int getSingleInt(String sqlText)
        {
            OracleCommand cmd = new OracleCommand();
            OracleConnection conn = new OracleConnection(strConn);
            try
            {

                PrepareCommand(cmd, conn, null, CommandType.Text, sqlText, null);
                //执行查询
                OracleDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                reader.Read();
                if (reader.IsDBNull(0))
                    return 0;
                else
                    return reader.GetInt32(0);
            }
            catch (Exception e)
            {
                conn.Close();
                return -1;
            }
            finally { conn.Close(); }
        }

        /// <summary>
        /// 查询返回一个结果集
        /// </summary>
        /// <param name="connString">连接字符串</param>
        //// <param name="commandType">命令类型（sql或者存储过程）</param>
        /// <param name="commandText">sql语句或者存储过程名称</param>
        /// <param name="commandParameters">命令所需参数数组</param>
        /// <returns></returns>
        public static OracleDataReader ExecuteReader( string cmdText)
        {

            // 创建一个OracleCommand
            OracleCommand cmd = new OracleCommand();
            // 创建一个OracleConnection
            OracleConnection conn = new OracleConnection(strConn);
            try
            {
                //调用静态方法PrepareCommand完成赋值操作
                PrepareCommand(cmd, conn, null, CommandType.Text, cmdText, null);
                //执行查询
                OracleDataReader odr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                //清空参数
                cmd.Parameters.Clear();
                return odr;

            }
            catch
            {
                //如果发生异常，关闭连接，并且向上抛出异常
                conn.Close();
                throw;
            }
        }

        /// <summary>
        /// 一个静态的预处理函数
        /// </summary>
        /// <param name="cmd">存在的OracleCommand对象</param>
        /// <param name="conn">存在的OracleConnection对象</param>
        /// <param name="trans">存在的OracleTransaction对象</param>
        /// <param name="cmdType">命令类型（sql或者存在过程）</param>
        /// <param name="cmdText">sql语句或者存储过程名称</param>
        /// <param name="commandParameters">Parameters for the command</param>
        private static void PrepareCommand(OracleCommand cmd, OracleConnection conn, OracleTransaction trans, CommandType cmdType, string cmdText, OracleParameter[] commandParameters)
        {

            //如果连接未打开，先打开连接
            if (conn.State != ConnectionState.Open)
                conn.Open();

            //未要执行的命令设置参数
            cmd.Connection = conn;
            cmd.CommandText = cmdText;
            cmd.CommandType = cmdType;

            //如果传入了事务，需要将命令绑定到指定的事务上去
            if (trans != null)
                cmd.Transaction = trans;

            //将传入的参数信息赋值给命令参数
            if (commandParameters != null)
            {
                cmd.Parameters.AddRange(commandParameters);
            }
        }


        /// <summary>
        /// 执行多条Sql获取返回值
        /// </summary>
        /// <param name="listSelectSql"></param>
        /// <returns></returns>
        //public DataSet Query(List<string> listSelectSql)
        //{
        //    using (OracleConnection conn = new OracleConnection(strConn))
        //    {


        //        OracleCommand cmd = new OracleCommand();
        //        StringBuilder strSql = new StringBuilder();
        //        strSql.Append("begin ");
        //        for (int i = 0; i < listSelectSql.Count; i++)
        //        {
        //            string paraName = "p_cursor_" + i;
        //            strSql.AppendFormat("open :{0} for {1}; ", paraName, listSelectSqli);
        //            cmd.Parameters.Add(paraName, OracleDbType.RefCursor, DBNull.Value, ParameterDirection.Output);
        //        }
        //        strSql.Append("end;");
        //        cmd.Connection = conn;
        //        cmd.CommandText = strSql.ToString();
        //        OracleDataAdapter oda = new OracleDataAdapter();
        //        oda.SelectCommand = cmd;
        //        conn.Open();
        //        DataSet ds = new DataSet();
        //        oda.Fill(ds);
        //        conn.Close();
        //        return ds;
        //    }
        //}

    }
}