﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Yunlib.Entity;
using YunLib.Common;
using Yunlib.Common;
using Oracle.ManagedDataAccess.Client;
using Yunlib.Extensions;

namespace Yunlib.DAL
{
    public class ReturnOrBrDAL
    {
        OracleConnection conn = new OracleConnection(OracleHelper.strConn);
        /// <summary>
        /// 通过条形码获取书和借阅信息详情
        /// </summary>
        /// <returns></returns>
        public BooksInfo GetBooksInfoByBarcode(string barCode)
        {
            BooksInfo model = new BooksInfo();
            #region 1
            StringBuilder strSql = new StringBuilder();
            //申明读者条码,允许续借次数,续借期限

           
            strSql.AppendLine(@" SELECT DISTINCT a.题名 as BooksName,");
            strSql.AppendLine(@"        e.姓名 as ReaderName,");
            strSql.AppendLine(@"        e.级别代码 as ClassCode, ");
            strSql.AppendLine(@"        e.单位代码 as CellCode,");
            strSql.AppendLine(@"        e.可外借 as CanBorrowCount,");
            strSql.AppendLine(@"        e.已外借 as BorrowEDCount,");
            strSql.AppendLine(@"        a.索书号 as SuoBookNum,");
            strSql.AppendLine(@"        c.条形码 as BarCode,");
            strSql.AppendLine(@"        c.主键码 as ID,");
            strSql.AppendLine(@"        d.读者条码 as ReaderCode,");
            strSql.AppendLine(@"        d.外借时间 as BorrowDate,");
            strSql.AppendLine(@"        d.应归还时间 as ShouldReturnDate");
            strSql.AppendLine(@" FROM 馆藏书目库 a");
            strSql.AppendLine(@" LEFT JOIN 采购库 b on a.主键码 = b.主键码");
            strSql.AppendLine(@" LEFT JOIN 馆藏典藏库 c on b.子键码 = c.子键码");
            strSql.AppendLine(@" LEFT JOIN 流通库 d on d.条形码=c.条形码");
            strSql.AppendLine(@" LEFT JOIN 读者库 e on e.读者条码=d.读者条码");
            strSql.AppendLine(@" LEFT JOIN 流通日志 f on  f.读者条码=d.读者条码");
            strSql.AppendFormat(@" WHERE c.条形码='{0}'".FormatWith(barCode));


            LogManager.ErrorRecord("qqq");
            //获取数据
            IDataReader dr = OracleHelper.ExecuteReader(strSql.ToString());
            LogManager.ErrorRecord("www");
            if (dr.Read())
            {
                model.ReaderName = dr["ReaderName"].ToString();
                
                model.CanBorrowCount = (dr["CanBorrowCount"].ToString() == "") ? 0 : Convert.ToInt32(dr["CanBorrowCount"].ToString());
                model.BorrowCountED = (dr["BorrowEDCount"].ToString() == "") ? (short)0 : Convert.ToInt16(dr["BorrowEDCount"].ToString());
                model.BooksName = dr["BooksName"].ToString();
                model.SuoBookNum = dr["SuoBookNum"].ToString();
                model.BarCode = dr["BarCode"].ToString();
                model.ID = Convert.ToInt32(dr["ID"].ToString());
                model.ReaderCode = dr["ReaderCode"].ToString();
                model.BorrowDate = (dr["BorrowDate"].ToString() == "") ? DateTime.Parse("1990-1-1") : Convert.ToDateTime(dr["BorrowDate"].ToString());
                model.ShouldReturnDate = (dr["ShouldReturnDate"].ToString() == "") ? DateTime.Parse("1990-1-1") : Convert.ToDateTime(dr["ShouldReturnDate"].ToString());
            }
            LogManager.ErrorRecord("eee");
            conn.Close();
            string dz_code = getDzByBarCode(barCode);
            LogManager.ErrorRecord("rrr");
            if (!string.IsNullOrWhiteSpace(dz_code))
            {
                LogManager.ErrorRecord("ttt");
                int AllowOnceBorrowCount = getBorrowCountNow(dz_code);
                LogManager.ErrorRecord("yyy");
                DataTable dt1 = getOnceBorrowTerm(dz_code);
                LogManager.ErrorRecord("uuu");
                model.BorrowCountNow = (short)AllowOnceBorrowCount;
                model.AllowOnceBorrowCount = (dt1.Rows[0]["AllowOnceBorrowCount"].ToString() == "") ? (short)0 : Convert.ToInt16(dt1.Rows[0]["AllowOnceBorrowCount"].ToString());
                model.OnceBorrowTerm = (dt1.Rows[0]["OnceBorrowTerm"].ToString() == "") ? (short)0 : Convert.ToInt16(dt1.Rows[0]["OnceBorrowTerm"].ToString());
            }
            return model;
            #endregion

        }

        public string getDzByBarCode(string barCode)
        {
            StringBuilder strSql = new StringBuilder();

            //通过书的条形码查询出读者条码
            strSql.AppendLine(@" SELECT 读者条码 t ");
            strSql.AppendLine(@" FROM 流通库 t WHERE  条形码='{0}'".FormatWith(barCode));
            DataTable dt = OracleHelper.ExecToSqlGetTable(strSql.ToString());
            if (dt.Rows.Count > 0)
            {
                return dt.Rows[0][0].ToString();
            }
            else
            {
                return "";
            }

        }

        public int getBorrowCountNow(string dz_code)
        {
            StringBuilder strSql = new StringBuilder();

            strSql.AppendLine(@" SELECT COUNT(*) as BorrowCountNow FROM 流通日志 t");
            strSql.AppendLine(@" WHERE 读者条码= '{0}'".FormatWith(dz_code));
            strSql.AppendLine(@" AND 操作类型='X'");
            DataTable dt = OracleHelper.ExecToSqlGetTable(strSql.ToString());
            return int.Parse(dt.Rows[0][0].ToString());

        }

        public DataTable getOnceBorrowTerm(string dz_code)
        {
            StringBuilder strSql = new StringBuilder();

            strSql.AppendLine(@" SELECT DISTINCT a.允许续借次数 as AllowOnceBorrowCount,");
            strSql.AppendLine(@"                 a.续借期限 as OnceBorrowTerm");
            strSql.AppendLine(@" FROM 流通参数定义 a");
            strSql.AppendLine(@" LEFT JOIN 读者库 b on ");
            strSql.AppendLine(@" a.级别代码=b.级别代码");
            strSql.AppendLine(@" WHERE b.读者条码='{0}'".FormatWith(dz_code));
            DataTable dt = OracleHelper.ExecToSqlGetTable(strSql.ToString());
            return dt;

        }


        public DataTable GetNewBooksInfoByBarcode(string barCode)
        {
            var sqlstr = @" SELECT a.条形码 as BOOKCODE,a.刊价 as PRICE,b.索书号 as CLASSIFICATION_NUM, b.题名 as TITLE, b.责任者 as AUTHOR, b.出版者 as PUBLISHER,b.出版地 as PUBLISHERPLACE, b.出版日期 as PUBDATE, b.标准编码 as ISBN, b.语种 as LAGS, b.图象页数 as PAGES, b.版次 as EDITION FROM  馆藏典藏库 a  left join 馆藏书目库  b on a.主键码 = b.主键码 where a.条形码 = '{0}'".FormatWith(barCode);
            DataTable dt = OracleHelper.ExecToSqlGetTable(sqlstr.ToString());

            return dt;
        }

        public DataTable GetNewBooksInfoByPage(string searchType,string searchStr,int page,int pageSize)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.AppendLine(@" select * from(SELECT a.条形码 as BOOKCODE,a.刊价 as PRICE,SUBSTR(b.索书号,1,1) as CLASSIFICATION_NUM, b.题名 as TITLE, b.责任者 as AUTHOR, b.出版者 as PUBLISHER,b.出版地 as PUBLISHERPLACE, 
	b.出版日期 as PUBDATE, b.标准编码 as ISBN, b.语种 as LAGS, b.图象页数 as PAGES, b.版次 as EDITION,rownum rn FROM 馆藏典藏库 a 
	 left join 馆藏书目库 b on a.主键码 = b.主键码 where");
            if(searchType == "1")
            {
                strSql.AppendLine(@" b.题名 like '%{0}%'".FormatWith(searchStr));
            }else if (searchType == "2")
            {
                strSql.AppendLine(@" b.责任者 like '%{0}%'".FormatWith(searchStr));
            }
            else if (searchType == "3")
            {
                strSql.AppendLine(@" b.标准编码 like '%{0}%'".FormatWith(searchStr));
            }
            else if (searchType == "4")
            {
                strSql.AppendLine(@" b.索书号 like '%{0}%'".FormatWith(searchStr));
            }

            strSql.AppendLine(@" AND a.条形码 not in (SELECT c.条形码  FROM 流通库 c left join 馆藏典藏库 a on c.条形码 = a.条形码
	 left join 馆藏书目库  b on a.主键码 = b.主键码 WHERE ");
            if (searchType == "1")
            {
                strSql.AppendLine(@" b.题名 like '%{0}%' )".FormatWith(searchStr));
            }
            else if (searchType == "2")
            {
                strSql.AppendLine(@" b.责任者 like '%{0}%' )".FormatWith(searchStr));
            }
            else if (searchType == "3")
            {
                strSql.AppendLine(@" b.标准编码 like '%{0}%' )".FormatWith(searchStr));
            }
            else if (searchType == "4")
            {
                strSql.AppendLine(@" b.索书号 like '%{0}%' )".FormatWith(searchStr));
            }
            strSql.AppendLine(@" )  where rn between {0} and {1}".FormatWith((page - 1) * pageSize, page
                    * pageSize));
            DataTable dt = OracleHelper.ExecToSqlGetTable(strSql.ToString());

            return dt;
        }

        public DataTable GetUserLendByDzCode(string dzCode)
        {
            var sqlstr = @" SELECT a.条形码 as BOOKCODE,a.刊价 as PRICE,substring(b.索书号,1,1) as CLASSIFICATION_NUM, b.题名 as TITLE, b.责任者 as AUTHOR, b.出版者 as PUBLISHER,b.出版地 as PUBLISHERPLACE, b.出版日期 as PUBDATE, b.标准编码 as ISBN, b.语种 as LAGS, b.图象页数 as PAGES, b.版次 as EDITION FROM 流通库 c left join 馆藏典藏库 as a on c.条形码 = a.条形码 left join 馆藏书目库 as b on a.主键码 = b.主键码 where c.读者条码 = '{0}'".FormatWith(dzCode);
            DataTable dt = OracleHelper.ExecToSqlGetTable(sqlstr.ToString());

            return dt;
        }
        /// <summary>
        /// 续借成功后,添加记录到流通日志中
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool InsertLTKOneInfo(ChangeLogInfo model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.AppendLine(@" INSERT INTO 流通日志");
            strSql.AppendLine(@"        (操作类型,");
            strSql.AppendLine(@"         条形码,");
            strSql.AppendLine(@"         登录号,");
            strSql.AppendLine(@"         读者条码,");
            strSql.AppendLine(@"         处理时间,");
            strSql.AppendLine(@"         赔罚款,");
            strSql.AppendLine(@"         操作员,");
            strSql.AppendLine(@"         主键码");
            strSql.AppendLine(@"         )");
            strSql.AppendLine(@" VALUES");
            strSql.AppendFormat(@"       ('{0}','{1}','{2}','{3}',to_date('{4}','yyyy/mm/dd hh24:mi:ss'),{5},{6},{7})", model.OperateType, model.BarCode, model.LoginAccount, model.ReaderCode, model.DealTime, model.Penalty, model.Operator, model.ID);

            return OracleHelper.ExecToSqlNonQuery(strSql.ToString()) > 0;
        }

        /// <summary>
        /// 通过条形码获取未还书籍
        /// </summary>
        /// <returns></returns>
        public ChangeKuInfo GetBorrowBooksInfoByBarcode(string barCode)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.AppendLine(@" SELECT 条形码 as BarCode,");
            strSql.AppendLine(@"        登录号 as LoginAccount,");
            strSql.AppendLine(@"        读者条码 as ReaderCode,");
            strSql.AppendLine(@"        外借时间 as BorrowTime,");
            strSql.AppendLine(@"        应归还时间 as ShouldReturnTime,");
            strSql.AppendLine(@"        主键码 as ID,");
            strSql.AppendLine(@"        虚拟库室 as DummyKu,");
            strSql.AppendLine(@"        续借次数 as OnceBorrowCount,");
            strSql.AppendLine(@"        处理时间 as DealTime,");
            strSql.AppendLine(@"        流通库室键码 as LTKSID,");
            strSql.AppendLine(@"        签到键码 as QDID");
            strSql.AppendLine(@" FROM 流通库");
            strSql.AppendFormat(@" WHERE 条形码='{0}'".FormatWith(barCode));

            IDataReader dr = OracleHelper.ExecuteReader(strSql.ToString());

            ChangeKuInfo model = null;

            if (dr.Read())
            {
                model = new ChangeKuInfo();
                model.BarCode = dr["BarCode"].ToString();
                model.LoginAccount = dr["LoginAccount"].ToString();
                model.ReaderCode = dr["ReaderCode"].ToString();
                model.BorrowTime = Convert.ToDateTime(dr["BorrowTime"].ToString());
                model.ShouldReturnTime = Convert.ToDateTime(dr["ShouldReturnTime"].ToString());
                model.ID = Convert.ToInt32(dr["ID"].ToString());
                model.DummyKu = Convert.ToInt32(dr["DummyKu"].ToString());
                model.OnceBorrowCount = Convert.ToInt32(dr["OnceBorrowCount"].ToString());
            }

            conn.Close();
            return model;
        }

        public DataTable GetChangeInfoByBarcode(string barCode)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.AppendLine(@" SELECT 条形码 as BARCODE ,");
            strSql.AppendLine(@"        登录号 as LOGIN ,");
            strSql.AppendLine(@"        读者条码 as DZCODE  ,");
            strSql.AppendLine(@"        外借时间 as CHECKTIME ,");
            strSql.AppendLine(@"        应归还时间 as RETURNTIME ,");
            strSql.AppendLine(@"        主键码 as PRIMARYKEY,");
            strSql.AppendLine(@"        虚拟库室 as VIRTUALLIBRARYROOM,");
            strSql.AppendLine(@"        续借次数 as RENEWNUMBER ,");
            strSql.AppendLine(@"        处理时间 as PROCESSINGTIME ,");
            strSql.AppendLine(@"        流通库室键码 as LENDKEY ,");
            strSql.AppendLine(@"        签到键码 as SIGNIN");
            strSql.AppendLine(@" FROM 流通库");
            strSql.AppendFormat(@" WHERE 条形码='{0}'".FormatWith(barCode));

            DataTable dt = OracleHelper.ExecToSqlGetTable(strSql.ToString());

            
            return dt;
        }

        public DataTable GetChangeInfoByDzCode(string dzCode)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.AppendLine(@" SELECT 条形码 as BARCODE ,");
            strSql.AppendLine(@"        登录号 as LOGIN ,");
            strSql.AppendLine(@"        读者条码 as DZCODE  ,");
            strSql.AppendLine(@"        外借时间 as CHECKTIME ,");
            strSql.AppendLine(@"        应归还时间 as RETURNTIME ,");
            strSql.AppendLine(@"        主键码 as PRIMARYKEY,");
            strSql.AppendLine(@"        虚拟库室 as VIRTUALLIBRARYROOM,");
            strSql.AppendLine(@"        续借次数 as RENEWNUMBER ,");
            strSql.AppendLine(@"        处理时间 as PROCESSINGTIME ,");
            strSql.AppendLine(@"        流通库室键码 as LENDKEY ,");
            strSql.AppendLine(@"        签到键码 as SIGNIN");
            strSql.AppendLine(@" FROM 流通库");
            strSql.AppendFormat(@" WHERE 读者条码='{0}'".FormatWith(dzCode));

            DataTable dt = OracleHelper.ExecToSqlGetTable(strSql.ToString());


            return dt;
        }

        public DataTable GetChangeInfo(string dzCode,string barCode)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.AppendLine(@" SELECT 条形码 as BARCODE ,");
            strSql.AppendLine(@"        登录号 as LOGIN ,");
            strSql.AppendLine(@"        读者条码 as DZCODE  ,");
            strSql.AppendLine(@"        外借时间 as CHECKTIME ,");
            strSql.AppendLine(@"        应归还时间 as RETURNTIME ,");
            strSql.AppendLine(@"        主键码 as PRIMARYKEY,");
            strSql.AppendLine(@"        虚拟库室 as VIRTUALLIBRARYROOM,");
            strSql.AppendLine(@"        续借次数 as RENEWNUMBER ,");
            strSql.AppendLine(@"        处理时间 as PROCESSINGTIME ,");
            strSql.AppendLine(@"        流通库室键码 as LENDKEY ,");
            strSql.AppendLine(@"        签到键码 as SIGNIN");
            strSql.AppendLine(@" FROM 流通库");
            strSql.AppendFormat(@" WHERE 读者条码='{0}'".FormatWith(dzCode));
            strSql.AppendFormat(@" AND 条形码='{0}'".FormatWith(barCode));

            DataTable dt = OracleHelper.ExecToSqlGetTable(strSql.ToString());


            return dt;
        }

        public DataTable GetBackLendLogByDzCode(string dz_code)
        {
            var sqlStr = @"select b.题名 as title,b.责任者 as author,a.条形码 as bookcode,a.处理时间 as processingtime,c.单位 
	                        from 流通日志 a left join 馆藏书目库 b on a.主键码 = b.主键码 left join 读者库 c on a.读者条码 = c.读者条码 
                                where a.读者条码 = '{0}' and a.操作类型 = 'H'".FormatWith(dz_code);
            DataTable dt = OracleHelper.ExecToSqlGetTable(sqlStr.ToString());
            
            return dt;
        }

        public bool UpdateChangeKUReBackTime(string barCode, DateTime time,int reBorrowNum)
        {
            string sql = @"update 流通库 set 应归还时间 =to_date('{0}','yyyy/mm/dd hh24:mi:ss'),续借次数={1}  where 条形码 = '{2}'".FormatWith(time,reBorrowNum, barCode);
            bool flag = OracleHelper.ExecToSqlNonQuery(sql.ToString()) > 0;

            return flag;
        }

        public bool updateBookTypeByBarCode(string barCode, int state)
        {
            string sql = @"update 馆藏典藏库 set 外借状态 = '{0}' where 条形码='{1}' ".FormatWith(state, barCode);
            bool flag = OracleHelper.ExecToSqlNonQuery(sql.ToString()) > 0;

            return flag;
        }


        public bool DeleteChangeInfoByBarCode(ChangeLogInfo model)
        {
            StringBuilder sqlStr = new StringBuilder();
            sqlStr.AppendLine(@" DELETE FROM 流通库");
            sqlStr.AppendFormat(@" WHERE 条形码='{0}'".FormatWith(model.BarCode));

            bool insertCmd = new ReturnOrBrDAL().InsertLTKOneInfo(model);
            if (!insertCmd)
            {
                //如果新增流通日志失败,就直接返回false
                return false;
            }

            bool deleteCmd = OracleHelper.ExecToSqlNonQuery(sqlStr.ToString()) > 0;

            bool res = false;

            //如果新增日志成功,但是删除流通库失败,就把新增成功的日志删除掉
            if (insertCmd && !deleteCmd)
            {
                StringBuilder builder = new StringBuilder();
                builder.AppendLine(@" DELETE FROM 流通日志");
                builder.AppendLine(@" WHERE ");
                builder.AppendLine(@" 处理时间=(");
                builder.AppendLine(@"           SELECT 处理时间 FROM 流通日志");
                builder.AppendLine(@"           WHERE ");
                builder.AppendFormat(@"         读者条码='{0}' ORDER BY 处理时间 DESC) ".FormatWith(model.ReaderCode));
                builder.AppendFormat(@" AND 读者条码='{0}'".FormatWith(model.ReaderCode));

                res = OracleHelper.ExecToSqlNonQuery(builder.ToString()) > 0;
            }
            //删除新增的流通日志成功,但是却没有操作成功
            if (res)
            {
                return false;
            }

            //如果新增流通日志和删除流通库都成功,才返回操作成功
            if (deleteCmd && insertCmd)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public int GetOnceBorrowCount(string rdid)
        {
            StringBuilder strSql = new StringBuilder();

            //查询当前已续借次数
            strSql.AppendLine(@" SELECT COUNT(*) FROM 流通日志");
            strSql.AppendFormat(@" WHERE 读者条码='{0}'".FormatWith(rdid));
            strSql.AppendLine(@" AND 操作类型='X'");

            return OracleHelper.getSingleInt(strSql.ToString());

        }

        /// <summary>
        /// 根据条形码查询书籍
        /// </summary>
        /// <returns></returns>
        public LocalBooks GetBookByBarCode(string barCode)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.AppendLine(@" SELECT c.条形码 as BarCode,");
            strSql.AppendLine(@"        a.题名 as BName,");
            strSql.AppendLine(@"        a.marc as MARC,");
            strSql.AppendLine(@"        a.责任者 as Writer,");
            strSql.AppendLine(@"        a.出版者 as Press,");
            strSql.AppendLine(@"        a.出版日期 as PublicationDate,");
            strSql.AppendLine(@"        a.索书号 as Classify");
            strSql.AppendLine(@" FROM 馆藏书目库 a");
            strSql.AppendLine(@" LEFT JOIN 采购库 b ON a.主键码 = b.主键码");
            strSql.AppendLine(@" LEFT JOIN 馆藏典藏库 c ON b.子键码 = c.子键码");
            strSql.AppendLine(@" WHERE 条形码 ='{0}'".FormatWith(barCode));

            DataTable dt = OracleHelper.ExecToSqlGetTable(strSql.ToString());

            IList<LocalBooks> list = DataTable2List<LocalBooks>.ConvertToModel(dt);

            if (list.Count > 0 && list != null)
            {
                return list[0];
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// 流通库插入数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Boolean AddCirculation(ChangeKuInfo model)
        {
            StringBuilder str = new StringBuilder();
            str.AppendLine(@"INSERT INTO 流通库
                   (条形码
                   ,登录号
                   ,读者条码
                   ,外借时间
                   ,应归还时间
                   ,主键码
                   ,虚拟库室
                   ,续借次数)
             VALUES ");
            str.AppendFormat(@" ('{0}','{1}','{2}',to_date('{3}','yyyy/mm/dd hh24:mi:ss'),to_date('{4}','yyyy/mm/dd hh24:mi:ss'),{5},{6},{7})", model.BarCode, model.LoginAccount, model.ReaderCode, model.BorrowTime, model.ShouldReturnTime,
                    model.ID, 1, model.OnceBorrowCount);
            string ss = str.ToString();
            int i = OracleHelper.ExecToSqlNonQuery(str.ToString());
            if (i > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 根据UserId获取用户已经借阅的信息
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public DataTable GetCirculationByUserId(string userId)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.AppendLine(@" SELECT 条形码 as BarCode,");
            strSql.AppendLine(@"        登录号 as LoginAccount,");
            strSql.AppendLine(@"        读者条码 as ReaderCode,");
            strSql.AppendLine(@"        外借时间 as BorrowTime,");
            strSql.AppendLine(@"        应归还时间 as ShouldReturnTime,");
            strSql.AppendLine(@"        主键码 as ID,");
            strSql.AppendLine(@"        虚拟库室 as DummyKu,");
            strSql.AppendLine(@"        续借次数 as OnceBorrowCount,");
            strSql.AppendLine(@"        处理时间 as DealTime,");
            strSql.AppendLine(@"        流通库室键码 as LTKSID,");
            strSql.AppendLine(@"        签到键码 as QDID");
            strSql.AppendLine(@" FROM 流通库");
            strSql.AppendLine(@" WHERE 读者条码='{0}'".FormatWith(userId));

            return OracleHelper.ExecToSqlGetTable(strSql.ToString());
        }

        public string getMarc(string barCode) {
            StringBuilder strSql = new StringBuilder();
            strSql.AppendLine(@" SELECT a.marc");
            strSql.AppendLine(@" FROM 馆藏书目库 a");
            strSql.AppendLine(@" LEFT JOIN 采购库 b ON a.主键码 = b.主键码");
            strSql.AppendLine(@" LEFT JOIN 馆藏典藏库 c ON b.子键码 = c.子键码");
            strSql.AppendLine(@" WHERE 条形码 ='{0}'".FormatWith(barCode));
            var dt = OracleHelper.ExecToSqlGetTable(strSql.ToString());
            return dt.Rows[0][0].ToString();
        }

    }
}
