﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Yunlib.Common
{
    public class MethodHelper
    {
        #region XML的对象序列化与反序列化
        public static string Serializer<T>(T obj, string encode = "utf-8")
            where T : class , new()
        {
            string result = string.Empty;

            try
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    System.Xml.Serialization.XmlSerializer xmlSerializer = new System.Xml.Serialization.XmlSerializer(typeof(T));

                    xmlSerializer.Serialize(ms, obj);

                    result = Encoding.GetEncoding(encode).GetString(ms.ToArray());
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return result;
        }

        public static T Deserialize<T>(string xmlStr)
            where T : class , new()
        {
            T result = default(T);

            try
            {
                using (StringReader reader = new StringReader(xmlStr))
                {
                    System.Xml.Serialization.XmlSerializer xmlSerializer = new System.Xml.Serialization.XmlSerializer(typeof(T));

                    result = xmlSerializer.Deserialize(reader) as T;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return result;
        }

        public static T DeserializeForReturnObject<T>(XmlNode xNode)
            where T : class , new()
        {
            T result = default(T);

            try
            {
                XmlNode cloneNode = xNode.CloneNode(true);

                XmlNode data = cloneNode.SelectSingleNode("ResultData");

                //返回数据为空,说明有错误,直接抛出错误描述
                if (data == null)
                {
                    int resultCode = cloneNode.SelectSingleNode("ResultCode").InnerText.ConvertTo<int>();

                    if (resultCode == ResultCodeStruct.Normal)
                    {
                        using (StringReader reader = new StringReader(xNode.OuterXml))
                        {
                            System.Xml.Serialization.XmlSerializer xmlSerializer = new System.Xml.Serialization.XmlSerializer(typeof(T));

                            result = xmlSerializer.Deserialize(reader) as T;
                        }

                        return result;
                    }
                    else
                    {
                        string msg = cloneNode.SelectSingleNode("ResultMessage").InnerText;

                        throw new Exception(msg);
                    }
                }
                else
                    xNode.SelectSingleNode("ResultData").InnerText = data.InnerXml.IndexOf("&lt;") >= 0 ? data.InnerText : data.InnerXml;

                using (StringReader reader = new StringReader(xNode.OuterXml))
                {
                    System.Xml.Serialization.XmlSerializer xmlSerializer = new System.Xml.Serialization.XmlSerializer(typeof(T));

                    result = xmlSerializer.Deserialize(reader) as T;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return result;
        }

        #endregion

        /// <summary>
        /// 克隆一个对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="RealObject"></param>
        /// <returns></returns>
        public static T Clone<T>(T RealObject)
        {
            using (Stream stream = new MemoryStream())
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(T));
                serializer.Serialize(stream, RealObject);
                stream.Seek(0, SeekOrigin.Begin);
                return (T)serializer.Deserialize(stream);
            }
        }

        public static string GetAppsettingsValue(string key)
        {
           return ConfigurationManager.AppSettings[key].ToString();
        }
    }
}
