﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Yunlib.Common
{
    [Serializable, JsonObject]
    public class RetuenData
    {
        public int ResultCode { get; set; }

        public string ResultMessage { get; set; }

        public dynamic ResultData { get; set; }

        public bool HasError
        {
            get
            {
                return ResultCode != ResultCodeStruct.Normal;
            }
        }
    }

    public struct ResultCodeStruct
    {
        //正常
        public const int Normal = 0;

        //错误
        public const int Error = 1;
    }
}
