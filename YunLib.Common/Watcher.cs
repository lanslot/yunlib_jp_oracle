﻿using System.Configuration;
using System.Text;
using ZooKeeperNet;

namespace Yunlib.Common
{
    public class Watcher : IWatcher
    {

        public void Process(WatchedEvent @event)
        {

            if (@event.Type == EventType.NodeDataChanged)
            {
                //  var stat = ZooKeeperClient.zk.Exists("/waylib/waylibroot/childone", this);
                //获取节点路径
                string zPath = ConfigurationManager.AppSettings["ZooKeeperPath"];
                //获取机器编号
                string machineID = ConfigurationManager.AppSettings["MachineId"];

                string path = zPath + "/" + machineID;
                //获取节点修改后的数据，绑定监听
                var data = ZooKeeperClient.zk.GetData(path, ZooKeeperClient.wc,null);
                //获取的数据转换为string类型
                var datastr = Encoding.Default.GetString(data);

                var sdata = datastr.Split('-');

                if (sdata[0] == "1") //借书成功打卡书柜
                {

                }
                else if (sdata[0] == "2") {//还书成功，打开书柜


                }


            }


            if (@event.State == KeeperState.Expired)
            {
                //重新建立连接
                ZooKeeperClient.content();
            }


        }

    }
}