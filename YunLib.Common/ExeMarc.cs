﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YunLib.Common
{
    public class ExeMarc
    {
        private static StreamReader FileReadStream;
        private static System.IO.FileStream CheckStream;
        private static string FilePath;
        public ExeMarc()
        {
            //
            // TODO: 在此处添加构造函数逻辑
            //
        }
        public DataTable MarcTodt(string Path)
        {
            DataTable dt = new DataTable();
            DataColumn ISBN = new DataColumn("ISBN", System.Type.GetType("System.String"));
            dt.Columns.Add(ISBN);
           // dt.Columns.Add("ID", System.Type.GetType("System.String"));
           // DataColumn ID = new DataColumn("ID", System.Type.GetType("System.String"));

            dt.Columns.Add("书名", System.Type.GetType("System.String"));
            dt.Columns.Add("Author", System.Type.GetType("System.String"));

            dt.Columns.Add("Price", System.Type.GetType("System.String"));
            dt.Columns.Add("Press", System.Type.GetType("System.String"));
            dt.Columns.Add("PressYear", System.Type.GetType("System.String"));
            dt.Columns.Add("Abstract", System.Type.GetType("System.String"));
            //dt.Columns.Add("Number",
            DataColumn[] pris = new DataColumn[1];

            //CheckStream = new FileStream(Path, FileMode.Open, FileAccess.Read, FileShare.Read);
            //FileReadStream = new StreamReader(Path, System.Text.Encoding.Default);
            //FilePath = Path;
            string BuffStr;
            int CurrLine = 1;
            //while ((int)(FileReadStream.Peek()) >= 0)
            //{
            try
            {
                BuffStr = Path;  //FileReadStream.ReadLine();//读取FileReadStream中一行字符，并赋值给BuffStr
                if (BuffStr.Length > 0)
                {
                    dt.Rows.Add(GetData(BuffStr)); //加入到dt中
                    CurrLine++;

                }
            }
            catch (Exception ex) 
                {

                    CurrLine--;
                }
            //}
            //FileReadStream.Close();
         //   dt.TableName = Path;
            return dt;
        }
        private string[] GetData(string IndexRecord)
        {
            if (IndexRecord.Length == 0)
            {
                return new string[8];
            }
            string ID = "无"; //识别码
            string BookName = "无"; //书名
            string Author = "无"; //作者
            string ISBN = "无";  //ISBN
            string Price = "无"; //价格
            string Press = "无"; //出版社
            string PressYear = "无"; //出版年
            string Abstract = "无"; //摘要
            //int IndexLen = int.Parse(Buff.Substring(12, 5)) - 25;
           // string IndexRecord = Buff.Substring(45); //目次区内容
           // int Data_Address = int.Parse(Buff.Substring(12, 5)); //数据区地址
                                                                 //ISBN
            ISBN = GetRecord("010", "a", IndexRecord).Trim();
            //识别码
           // ID = GetRecord("001", "", IndexRecord).Trim();
            //书名
            BookName = GetRecord( "200", "a", IndexRecord).Trim();
            //作者
            Author = GetRecord(  "200", "f", IndexRecord).Trim();

            //价格
            Price = GetRecord(  "010", "d", IndexRecord).Trim();
            //出版社
            Press = GetRecord(  "210", "c", IndexRecord).Trim();
            //出版年
            PressYear = GetRecord(  "210", "d", IndexRecord).Trim();
            //摘要
            Abstract = GetRecord(  "330", "a", IndexRecord).Trim();
            string[] ret = { ISBN, BookName, Author, Price, Press, PressYear, Abstract };
            return ret;
        }
        /// <summary>
        /// 返回Buff中的字段号为 Member， 子字段号为 ZiMember 的数据
        /// </summary>
        /// <param name="Buff"></param>
        /// <param name="Member">字段号</param>
        /// <param name="SubMember">子字段号</param>
        /// <param name="Data_Addr">地址区基地址</param>
        /// <returns></returns>
        public string GetRecord(string Member, string SubMember, string IndexRecord)
        {
            string dataValue = "";
            var datas = IndexRecord.Split('\u001e');
            
            
            for (int i = 3; i < datas.Length; i++)
            {
                var newMember = datas[i].Substring(0,3);
                var memberValue = datas[i].Substring(3);
                if (Member == newMember)
                {
                    var childData = memberValue.Split('\u001f');

                    for (int j = 0; j < childData.Length; j++) {
                        var newSubMember = childData[j].Substring(0, 1);
                        if (SubMember == newSubMember) {
                            dataValue = childData[j].Substring(1);
                            break;
                        }
                    }
                        //子记录相对于数据区偏移地址
                        //  dataValue = childData
                        break;
                }
            }
            return dataValue;
        }

        /// <summary>
        /// 计算由汉字引起的偏移地址误差，返回汉字个数
        /// </summary>
        ///<param name="Buff">待统计的字符串</param>
        ///<param name="StartIndex">开始位置</param>
        ///<param name="EndIndex">截至位置</param>
        /// <returns></returns>
        public int GetOffSet(string Buff, int StartIndex, int EndIndex)
        {
            char[] temp = Buff.ToCharArray();
            int LetterCount = 0, HanZiCount = 0;
            for (int i = StartIndex; i < EndIndex; i++)
            {
                int test = (int)temp[i];
                if (test < 128)
                {
                    LetterCount++;
                }
                else
                {
                    HanZiCount++;
                }
                if (HanZiCount * 2 + LetterCount == EndIndex - StartIndex)
                {
                    return HanZiCount;
                }
            }
            return HanZiCount;
        }

    }
}
