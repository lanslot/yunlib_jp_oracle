﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Data;

namespace Yunlib.Common
{ 
    public static class JObjectExtensions
    {
        /// <summary>
        /// 判断JObject是否有指定name的属性
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static bool HasAttribute(this JObject obj, string name)
        {
            return obj[name] != null;
        }

        /// <summary>
        /// 判断返回数据是否有错
        /// </summary>
        public static bool HasError(this JObject obj)
        {
            if (obj.HasAttribute("ResultCode"))
                return obj["ResultCode"].Value<int>() != 0;
            else
                throw new Exception("无效的接口返回数据格式.");
        }

        /// <summary>
        /// 获取错误消息
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string GetErrorMessage(this JObject obj)
        {
            if (obj.HasAttribute("ResultMessage"))
                return obj["ResultMessage"].Value<string>();
            else
                throw new Exception("无效的接口返回数据格式.");
        }

        /// <summary>
        /// 获取数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static T GetData<T>(this JObject obj)
        {
            if (obj.HasAttribute("ResultData"))
                return obj["ResultData"].ToObject<T>();
            else
                throw new Exception("无效的接口返回数据格式.");
        }

        /// <summary>
        /// 获取返回数据
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static RetuenData GetRetuenData(this JObject obj)
        {
            RetuenData rd = new RetuenData();
            if (obj.HasAttribute("ResultCode"))
                rd = obj.ToString().ToObject<RetuenData>();
            else
            {
                rd.ResultCode = ResultCodeStruct.Error;
                rd.ResultMessage = "{0}=>{1}".FormatWith(obj["Message"], obj["MessageDetail"]);
            }

            return rd;
        }
        static public bool ObjectIsNull(Object obj)
        {
            //如果对象引用为null 或者 对象值为null 或者对象值为空
            if (obj == null || obj == DBNull.Value || obj.ToString().Equals("") || obj.ToString() == "")
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// 表转集合
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="table"></param>
        /// <returns></returns>
        public static List<T> GetEntities<T>(this DataTable table) where T : new()
        {
            List<T> entities = new List<T>();
            foreach (DataRow row in table.Rows)
            {
                T entity = new T();
                foreach (var item in entity.GetType().GetProperties())
                {
                    if (row.Table.Columns.Contains(item.Name) && DBNull.Value != row[item.Name])
                    {
                        //item.SetValue(entity, Convert.ChangeType(row[item.Name],  item.PropertyType), null);
                        if (!ObjectIsNull(row[item.Name]))
                        {
                            if (item.PropertyType.FullName == "System.String")
                            {
                                item.SetValue(entity, Convert.ToString(row[item.Name]), null);
                            }
                            else if (item.PropertyType.FullName == "System.Int32")
                            {
                                item.SetValue(entity, Convert.ToInt32(row[item.Name]), null);
                            }
                            else if (item.PropertyType.FullName == "System.Int64")
                            {
                                item.SetValue(entity, Convert.ToInt64(row[item.Name]), null);
                            }
                            else if (item.PropertyType.FullName == "System.Single")
                            {
                                item.SetValue(entity, Convert.ToSingle(row[item.Name]), null);
                            }
                            else if (item.PropertyType.FullName == "System.Double")
                            {
                                item.SetValue(entity, Convert.ToDouble(row[item.Name]), null);
                            }
                            else if (item.PropertyType.FullName == "System.Decimal")
                            {
                                item.SetValue(entity, Convert.ToDecimal(row[item.Name]), null);
                            }
                            else if (item.PropertyType.FullName == "System.Char")
                            {
                                item.SetValue(entity, Convert.ToChar(row[item.Name]), null);
                            }
                            else if (item.PropertyType.FullName == "System.Boolean")
                            {
                                item.SetValue(entity, Convert.ToBoolean(row[item.Name]), null);
                            }
                            else if (item.PropertyType.FullName == "System.DateTime")
                            {
                                item.SetValue(entity, Convert.ToDateTime(row[item.Name]), null);
                            }
                            //可空类型
                            else if (item.PropertyType.FullName == "System.Nullable`1[[System.DateTime, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]")
                            {
                                item.SetValue(entity, Convert.ToDateTime(row[item.Name]), null);
                            }
                            else if (item.PropertyType.FullName == "System.Nullable`1[[System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]")
                            {
                                item.SetValue(entity, Convert.ToDateTime(row[item.Name]), null);
                            }
                            else if (item.PropertyType.FullName == "System.Nullable`1[[System.Int32, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]")
                            {
                                item.SetValue(entity, Convert.ToInt32(row[item.Name]), null);
                            }
                            else if (item.PropertyType.FullName == "System.Nullable`1[[System.Int32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]")
                            {
                                item.SetValue(entity, Convert.ToInt32(row[item.Name]), null);
                            }
                            else if (item.PropertyType.FullName == "System.Nullable`1[[System.Int64, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]")
                            {
                                item.SetValue(entity, Convert.ToInt64(row[item.Name]), null);
                            }
                            else if (item.PropertyType.FullName == "System.Nullable`1[[System.Int64, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]")
                            {
                                item.SetValue(entity, Convert.ToInt64(row[item.Name]), null);
                            }
                            else if (item.PropertyType.FullName == "System.Nullable`1[[System.Decimal, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]")
                            {
                                item.SetValue(entity, Convert.ToDecimal(row[item.Name]), null);
                            }
                            else if (item.PropertyType.FullName == "System.Nullable`1[[System.Decimal, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]")
                            {
                                item.SetValue(entity, Convert.ToDecimal(row[item.Name]), null);
                            }
                            else if (item.PropertyType.FullName == "System.Nullable`1[[System.Boolean, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]")
                            {
                                item.SetValue(entity, Convert.ToBoolean(row[item.Name]), null);
                            }
                            else if (item.PropertyType.FullName == "System.Nullable`1[[System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]")
                            {
                                item.SetValue(entity, Convert.ToBoolean(row[item.Name]), null);
                            }
                            else
                            {
                                throw new Exception("属性包含不支持的数据类型!");
                            }
                        }
                        else
                        {
                            item.SetValue(entity, null, null);
                        }
                    }
                }
                entities.Add(entity);
            }
            return entities;
        }
    }
}
