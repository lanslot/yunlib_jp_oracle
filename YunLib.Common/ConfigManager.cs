﻿using System;
using System.Configuration;

namespace YunLib.Common
{
    public static class ConfigManager
    {
        /// <summary>
        /// 当前控制台波特率
        /// </summary>
        public static int CmdPort
        {
            get
            {
                string cmdPort = ConfigurationManager.AppSettings["CmdPort"];

                if (string.IsNullOrWhiteSpace(cmdPort))
                    return 9600;
                else
                    return Convert.ToInt32(cmdPort);
            }
        }

        /// <summary>
        /// 当前IM刷卡波特率
        /// </summary>
        public static int IMPort
        {
            get
            {
                string imPort = ConfigurationManager.AppSettings["IMPort"];

                if (string.IsNullOrWhiteSpace(imPort))
                    return 115200;
                else
                    return Convert.ToInt32(imPort);
            }
        }

        /// <summary>
        /// 当前控制台串口号
        /// </summary>
        public static string Control
        {
            get
            {
                string control = ConfigurationManager.AppSettings["Control"];

                if (string.IsNullOrWhiteSpace(control))
                    return "COM3";
                else
                    return control;
            }
        }

        /// <summary>
        /// 当前控制台串口号
        /// </summary>
        public static string Scan
        {
            get
            {
                string scan = ConfigurationManager.AppSettings["Scan"];

                if (string.IsNullOrWhiteSpace(scan))
                    return "COM6";
                else
                    return scan;
            }
        }

        /// <summary>
        /// 当前图书管理员卡号
        /// </summary>
        public static string BooksManager
        {
            get
            {
                string booksManager = ConfigurationManager.AppSettings["BooksManager"];

                if (string.IsNullOrWhiteSpace(booksManager))
                    throw new Exception("请设置图书管理员卡号.");
                else
                    return booksManager;
            }
        }

        /// <summary>
        /// 书柜类型(210 or 240)
        /// </summary>
        public static string MachineElementType
        {
            get
            {
                string machineElementType = ConfigurationManager.AppSettings["MachineElementType"];

                if (string.IsNullOrWhiteSpace(machineElementType))
                    throw new Exception("请设置书柜格子数.");
                else
                    return machineElementType;
            }
        }

        /// <summary>
        /// 密码口令
        /// </summary>
        public static string LoginKey
        {
            get
            {
                string loginKey = ConfigurationManager.AppSettings["LoginKey"];

                if (string.IsNullOrWhiteSpace(loginKey))
                    throw new Exception("请设置密码口令.");
                else
                    return loginKey;
            }
        }
    }
}
