﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yunlib.Extensions
{
    public class LogManager
    {
        static object locker = new object();
        /// <summary>
        /// 重要信息写入日志
        /// </summary>
        /// <param name="logs">日志列表，每条日志占一行</param>
        public static void WriteProgramLog(params string[] logs)
        {
            lock (locker)
            {
                string LogAddress = "D:\\APILog";
                if (!Directory.Exists(LogAddress + "\\PRG"))
                {
                    Directory.CreateDirectory(LogAddress + "\\PRG");
                }
                LogAddress = string.Concat(LogAddress, "\\PRG\\",
                 DateTime.Now.Year, '-', DateTime.Now.Month, '-',
                 DateTime.Now.Day, "_program.log");
                StreamWriter sw = new StreamWriter(LogAddress, true);
                foreach (string log in logs)
                {
                    sw.WriteLine(string.Format("【{0}】 {1}", DateTime.Now.ToString(), log));
                }
                sw.Close();
            }
        }

        public static void BorrowRecord(params string[] logs)
        {
            lock (locker)
            {
                string LogAddress = Environment.CurrentDirectory + "\\Log";
                if (!Directory.Exists(LogAddress + "\\ZooKeeperLog"))
                {
                    Directory.CreateDirectory(LogAddress + "\\ZooKeeperLog");
                }
                LogAddress = string.Concat(LogAddress, "\\ZooKeeperLog\\",
                 DateTime.Now.Year, '-', DateTime.Now.Month, '-',
                 DateTime.Now.Day, "_ZooKeeperRecord.log");
                StreamWriter sw = new StreamWriter(LogAddress, true);
                foreach (string log in logs)
                {
                    sw.WriteLine(string.Format("【{0}】 {1}", DateTime.Now.ToString(), log));
                }
                sw.Close();
            }
        }

        public static void ErrorRecord(params string[] logs)
        {
            lock (locker)
            {
                string LogAddress = "D:\\Log";
                if (!Directory.Exists(LogAddress + "\\ErrorLog"))
                {
                    Directory.CreateDirectory(LogAddress + "\\ErrorLog");
                }
                LogAddress = string.Concat(LogAddress, "\\ErrorLog\\",
                 DateTime.Now.Year, '-', DateTime.Now.Month, '-',
                 DateTime.Now.Day, "_ErrorRecord.log");
                StreamWriter sw = new StreamWriter(LogAddress, true);
                foreach (string log in logs)
                {
                    sw.WriteLine(string.Format("【{0}】 {1}", DateTime.Now.ToString(), log));
                }
                sw.Close();
            }
        }
    }
}
