﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yunlib.Common
{
    public class Constant
    {
        /// <summary>
        /// 登录验证码Session键
        /// </summary>
        public const string VerifyCodeSessionKey = "cx_session_verifycode";

        /// <summary>
        /// 登录用户Cookie键
        /// </summary>
        public const string LoginUserCookieKey = "cx_loginuserkey_token_{0}";

        /// <summary>
        /// 登录用户角色数据键
        /// </summary>
        public const string LoginRolesCookieKey = "cx_roles_key_{0}";

        /// <summary>
        /// 保单区域缓存键
        /// </summary>
        public const string PolicyAreaCacheKey = "cx_policyarea_key";

        /// <summary>
        /// 投保城市缓存键
        /// </summary>
        public const string InsuranceCityCacheKey = "cx_InsuranceCity_key";

        /// <summary>
        /// 公告数据缓存key
        /// </summary>
        public const string AnnouncementCacheKey = "cx_Announcement_key";

        /// <summary>
        /// 网站标题
        /// </summary>
        public const string SiteTile = "车险后台管理系统";

        /// <summary>
        /// 系统管理员常量字符串
        /// </summary>
        public const string AdminUser = "系统管理员";

        /// <summary>
        /// 中科根级公司ID
        /// </summary>
        public const int TopLevelCompanyID = 965;
        /// <summary>
        /// 默认套餐名称-基础型
        /// </summary>
        public const string BaseType = "基本型套餐";
        /// <summary>
        /// 默认套餐名称-经济型
        /// </summary>
        public const string EconomicalType = "经济型套餐";
        /// <summary>
        /// 默认套餐名称-全面型
        /// </summary>
        public const string ComprehensiveType = "全面型套餐";
        /// <summary>
        /// 默认套餐名称-豪华型
        /// </summary>
        public const string LuxuryType = "豪华型套餐";
    }
}
