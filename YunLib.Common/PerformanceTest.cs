﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Yunlib.Common
{
    public class PerformanceTest
    {
        private static PerformanceTest _instance;
        private static readonly Object syncLock = new Object();
        /// <summary>  
        /// 单例模式  
        /// </summary>  
        public static PerformanceTest Instance
        {
            get
            {
                if (PerformanceTest._instance == null)
                {
                    lock (syncLock)
                    {
                        if (PerformanceTest._instance == null)
                        {
                            PerformanceTest._instance = new PerformanceTest();
                        }
                    }
                }
                return PerformanceTest._instance;
            }
        }

        private static DateTime BeginTime;
        private static DateTime EndTime;
        private static int _RunCount = 1;
        private static bool _IsMultithread = false;

        private static Stopwatch watch;

        [DllImport("kernel32.dll")]
        public static extern bool AllocConsole();

        static object lockObj = new object();

        public static bool EnableAllocConsole = false;

        [DebuggerStepThrough]
        static PerformanceTest()
        {
            if (EnableAllocConsole)
            {
                AllocConsole();

                Console.BufferWidth = 100;
                Console.WindowWidth = 100;
            }
        }

        /// <summary>
        ///设置执行次数(默认:1)
        /// </summary>
        public static int RunCount
        {
            set
            {
                _RunCount = value;
            }
            get
            {
                return _RunCount;
            }
        }

        /// <summary>
        /// 设置线程模式(默认:false)
        /// </summary>
        /// <param name="isMul">true为多线程</param>
        public static bool IsMultithread
        {
            set
            {
                _IsMultithread = value;
            }
            get
            {
                return _IsMultithread;
            }
        }


        /// <summary>
        /// 执行函数
        /// </summary>
        /// <param name="action"></param>
        [DebuggerStepThrough]
        public static void Execute(Action<int> action, Action<string> rollBack)
        {
            List<Thread> arr = new List<Thread>();
            BeginTime = DateTime.Now;
            for (int i = 0; i < RunCount; i++)
            {
                if (IsMultithread)
                {
                    var thread = new Thread(new System.Threading.ThreadStart(() =>
                    {
                        action(i);
                    }));
                    thread.Start();
                    arr.Add(thread);
                }
                else
                {
                    action(i);
                }
            }
            if (IsMultithread)
            {
                foreach (Thread t in arr)
                {
                    while (t.IsAlive)
                    {
                        Thread.Sleep(10);
                    }
                }

            }
            rollBack(GetResult());

            ReSet();
        }

        [DebuggerStepThrough]
        static void ReSet()
        {
            _RunCount = 1;
            _IsMultithread = false;
        }

        [DebuggerStepThrough]
        public static string GetResult()
        {
            EndTime = DateTime.Now;
            string totalTime = ((EndTime - BeginTime).TotalMilliseconds / 1000.0).ToString("n5");
            string reval = string.Format("总共执行时间：{0}秒", totalTime);
            return reval;
        }

        [Conditional("DEBUG")]
        [DebuggerStepThrough]
        public void Start(string startDesc = "", ConsoleColor cColor = ConsoleColor.Green)
        {
            watch = Stopwatch.StartNew();
            if (string.IsNullOrEmpty(startDesc))
                WriteLine(string.Format("{0}.{1}执行开始", MethodBase.GetCurrentMethod().ReflectedType.Name, new StackTrace().GetFrame(1).GetMethod().Name), cColor);
            else
                WriteLine(string.Format("{0}执行开始", startDesc ?? string.Empty), cColor);
        }

        [Conditional("DEBUG")]
        [DebuggerStepThrough]
        public void End(string endDesc = "", ConsoleColor cColor = ConsoleColor.Green)
        {
            watch.Stop();
            if (string.IsNullOrEmpty(endDesc))
                WriteLine(string.Format("{0}.{1}执行结束--耗费时间:{2}分{3}秒{4}毫秒", MethodBase.GetCurrentMethod().ReflectedType.Name, new StackTrace().GetFrame(1).GetMethod().Name, watch.Elapsed.Minutes, watch.Elapsed.Seconds, watch.Elapsed.Milliseconds), cColor);
            else
                WriteLine(string.Format("{0}执行结束--耗费时间:{1}分{2}秒{3}毫秒", endDesc ?? string.Empty, watch.Elapsed.Minutes, watch.Elapsed.Seconds, watch.Elapsed.Milliseconds), cColor);
        }

        [Conditional("DEBUG")]
        [DebuggerStepThrough]
        static void WriteLine(string message, ConsoleColor cColor)
        {
            lock (lockObj)
            {
                Console.ForegroundColor = cColor;
                Console.WriteLine(message);
                Console.ForegroundColor = ConsoleColor.White;
                Console.BackgroundColor = ConsoleColor.Black;
            }
        }
    }

}
