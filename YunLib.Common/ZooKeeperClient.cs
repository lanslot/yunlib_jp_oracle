﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZooKeeperNet;

namespace Yunlib.Common
{
    public class ZooKeeperClient
    {

        public static ZooKeeper zk;
        public static Watcher wc = new Watcher();

        /// <summary>
        /// 创建节点
        /// </summary>
        public void createNode() {
            //获取节点路径
            string zPath = ConfigurationManager.AppSettings["ZooKeeperPath"];
            //获取机器编号
            string machineID = ConfigurationManager.AppSettings["MachineId"];

            string path = zPath + "/" + machineID;

            //创建节点
            zk.Create(path, "childone".GetBytes(), Ids.OPEN_ACL_UNSAFE, CreateMode.Ephemeral);

            //绑定监听
            var stat = zk.Exists(path, wc);
        }

        /// <summary>
        /// 建立连接
        /// </summary>
        public static  void content() {
            //获取服务器IP
            string zIP = ConfigurationManager.AppSettings["ZooKeeperIP"];
            
            zk = new ZooKeeper(zIP, new TimeSpan(0, 0, 0, 1000), null);
        }
    }
}
