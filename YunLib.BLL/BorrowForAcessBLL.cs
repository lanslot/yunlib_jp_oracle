﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yunlib.DAL;
using Yunlib.Entity;

namespace YunLib.BLL
{
    public class BorrowForAcessBLL
    {
        BorrowForAcess DAL = new BorrowForAcess();

        public DataTable GetBookToDataTable()
        {
            return DAL.GetBookToDataTable();
        }

        public DataTable GetBookByDoorID(int doorId)
        {
            return DAL.GetBookByDoorID(doorId);
        }

        public IList<LocalBooks> GetBookByDoorNum(int doorNum)
        {
            return DAL.GetBookByDoorNum(doorNum);
        }

        public IList<LocalBooks> GetNullBookCase()
        {
            return DAL.GetNullBookCase();
        }

        public bool FillBookToAcess(LocalBooks lb)
        {
            return DAL.FillBookToAcess(lb);
        }

        public bool DeleteBookByID(LocalBooks lb) {
            return DAL.DeleteBookByID(lb);
        }

        public LocalBooks GetBookByBarCode(string barCode)
        {
            return DAL.GetBookByBarCode(barCode);
        }

        public int GetTotalCount()
        {
            return DAL.GetTotalCount();
        }

        public DataTable GetBookToDataTableByPaging(int pageIndex, int pageSize)
        {
            return DAL.GetBookToDataTableByPaging(pageIndex, pageSize);
        }
    }
}
