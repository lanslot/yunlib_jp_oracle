﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yunlib.DAL;

namespace YunLib.BLL
{
    public class BorrowBLL
    {
        BorrowDAL DAL = new BorrowDAL();

        public DataTable GetBookName()
        {
            return DAL.GetBookName();
        }
    }
}
