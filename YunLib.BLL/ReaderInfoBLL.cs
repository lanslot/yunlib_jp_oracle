﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yunlib.DAL;

namespace YunLib.BLL
{
    public class ReaderInfoBLL
    {
        ReaderInfoDAL DAL = new ReaderInfoDAL();

        public DataTable UserLogin(string dz_code )
        {
            return DAL.UserLogin(dz_code);
        }

        public DataTable GetDzByLendBookCode(string barcode)
        {
            return DAL.GetDzByLendBookCode(barcode);
        }
        public DataTable GetUser(string rdid)
        {
            return DAL.GetUser(rdid);
        }

        public DataTable GetUserByReaderCode(string rdid) {
            return DAL.GetUserByReaderCode(rdid);
        }

        public bool UpdateUserBoorowNum(int borrowCount,string username)
        {
            return DAL.UpdateUserBoorowNum(borrowCount,username);
        }
    }
}
