﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YunLib.DAL;

namespace YunLib.BLL
{
    public class UserRecordForBLL
    {
        UserLoanRecordForAcess DAL = new UserLoanRecordForAcess();

        public bool AddRecord(string userId, string bookId)
        {
            return DAL.AddRecord(userId, bookId);
        }

        public int GetUserRecordCount(string userId)
        {
            return DAL.GetUserRecordCount(userId);
        }

        public bool DeleteAll()
        {
            return DAL.DeleteAll();
        }

    }
}
