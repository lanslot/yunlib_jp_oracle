﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yunlib.DAL;
using Yunlib.Entity;

namespace YunLib.BLL
{
    public class ReturnOrBrBLL
    {
        ReturnOrBrDAL DAL = new ReturnOrBrDAL();

        public BooksInfo GetBooksInfoByBarcode(string barCode)
        {
            return DAL.GetBooksInfoByBarcode(barCode);
        }

        public DataTable GetNewBooksInfoByBarcode(string barCode)
        {
            return DAL.GetNewBooksInfoByBarcode(barCode);
        }

        public DataTable GetNewBooksInfoByPage(string searchType, string searchStr, int page, int pageSize)
        {
            return DAL.GetNewBooksInfoByPage(searchType, searchStr, page, pageSize);
        }

        public DataTable GetUserLendByDzCode(string dzCode)
        {
            return DAL.GetUserLendByDzCode(dzCode);
        }

        public bool InsertLTKOneInfo(ChangeLogInfo model)
        {
            return DAL.InsertLTKOneInfo(model);
        }

        public ChangeKuInfo GetBorrowBooksInfoByBarcode(string barCode)
        {
            return DAL.GetBorrowBooksInfoByBarcode(barCode);
        }

        public DataTable GetChangeInfoByBarcode(string barCode)
        {
            return DAL.GetChangeInfoByBarcode(barCode);
        }

        public DataTable GetChangeInfoByDzCode(string dzCode)
        {
            return DAL.GetChangeInfoByDzCode(dzCode);
        }

        public DataTable GetChangeInfo(string dzCode, string barCode)
        {
            return DAL.GetChangeInfo(dzCode, barCode);
        }

        public DataTable GetBackLendLogByDzCode(string dz_code)
        {
            return DAL.GetBackLendLogByDzCode(dz_code);
        }

        public bool UpdateChangeKUReBackTime(string barCode, DateTime time,int reBorrowNum) {
            return DAL.UpdateChangeKUReBackTime(barCode, time,reBorrowNum);
        }

        public bool updateBookTypeByBarCode(string barCode, int state)
        {
            return DAL.updateBookTypeByBarCode(barCode, state);
        }


        public bool DeleteChangeInfoByBarCode(ChangeLogInfo model)
        {
            return DAL.DeleteChangeInfoByBarCode(model);
        }
        public int GetOnceBorrowCount(string rdid)
        {
            return DAL.GetOnceBorrowCount(rdid);
        }

        public LocalBooks GetBookByBarCode(string barCode)
        {
            return DAL.GetBookByBarCode(barCode);
        }
        public Boolean AddCirculation(ChangeKuInfo model)
        {
            return DAL.AddCirculation(model);
        }

        public DataTable GetCirculationByUserId(string userId)
        {
            return DAL.GetCirculationByUserId(userId);
        }

    }
}
